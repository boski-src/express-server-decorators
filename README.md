#express-server-decorators

> Project provide typescript decorators for simple setup web server based on ExpressJS.

## Installation
```
npm install express-server-decorators --save
```
## Example
Look at the example (API Server): [click here](https://github.com/boski-src/ts-express-server-decorators/tree/master/examples)