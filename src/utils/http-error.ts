export class HttpError extends Error {
  public status;
  public isCustom;

  constructor (message : string, status : number) {
    super(message);
    this.status = status;
    this.isCustom = true;
  }
}